from contextlib import contextmanager
from io import StringIO
import sys
from typing import IO, Optional

from PROJECT_NAME.console import main


@contextmanager
def wrap_stdout_stderr(stdout: IO, stderr: IO):
    old_stdout = sys.stdout
    old_stderr = sys.stderr
    sys.stdout = stdout
    sys.stderr = stderr
    try:
        yield
    finally:
        sys.stdout = old_stdout
        sys.stderr = old_stderr


def wrap_main(args: list):
    stdout, stderr = StringIO(), StringIO()
    code = 0
    with wrap_stdout_stderr(stdout, stderr):
        try:
            main(args=args)
        except SystemExit as e:
            code = e.code
    result = [code, stdout.getvalue(), stderr.getvalue()]
    stdout.close()
    stderr.close()
    return result


def test_calc():
    code, stdout, stderr = wrap_main(["2"])
    assert code == 0
    assert stdout == "4\n"
    assert stderr == ""


def test_help():
    code, stdout, stderr = wrap_main(["--help"])
    assert code == 0
    assert "usage:" in stdout
    assert "help message and exit" in stdout
    assert stderr == ""


def test_version():
    code, stdout, stderr = wrap_main(["--version"])
    assert code == 0
    assert len(stdout) > 10
    assert stderr == ""


def test_unknown_option():
    code, stdout, stderr = wrap_main(["-x", "2"])
    assert code == 2
    assert stdout == ""
    assert "unrecognized arguments" in stderr


def test_no_arguments():
    code, stdout, stderr = wrap_main([])
    assert code == 2
    assert stdout == ""
    assert "arguments are required" in stderr
