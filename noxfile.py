import nox


@nox.session(python=["3.7", "3.8", "3.9", "3.10", "3.11"])
def test(session):
    session.install("pytest")
    session.install(".")
    session.run("pytest")


@nox.session(python=["3.11"])
def black(session):
    session.install("black")
    session.run("black", "--check", "--diff", ".")
