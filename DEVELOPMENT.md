## Development Guide

### Setup

1.  Install [pyenv](https://github.com/pyenv/pyenv) and
    [Poetry](https://python-poetry.org/).

2.  Clone the repository:

    ```sh
    git clone git@gitlab.com:jagaro/PROJECT-NAME.git
    cd PROJECT-NAME
    ```

3.  Install Python versions:

    ```sh
    pyenv install -s
    ```

4.  Install dependencies:

    ```sh
    poetry install
    ```

    If you get an error, you may need to set the following:

    ```sh
    export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
    ```

    See https://github.com/python-poetry/poetry/issues/1917.

### Test

- Run tests:

  ```sh
  poetry run python
  ```

- Continuous test watcher:

  ```sh
  poetry run ptw .
  ```

- Run all tests for all Python versions:

  ```sh
  poetry run nox
  ```

### Clean

- Remove all cached files:

  ```sh
  git clean -fx .
  ```

### New Version

1.  Set a new version:

    ```sh
    poetry version              # show current version
    poetry version NEW_VERSION  # set new version
    ```

2.  Commit, tag and push:

    ```sh
    VERSION="$(poetry version -s)"
    git commit -S -a -m "Version $VERSION"
    git tag -s -m "Version $VERSION" $VERSION
    git push --follow-tags
    ```

    If the tests pass, a new release is automatically created on [GitLab](https://gitlab.com/).

### Publish to PyPI

1.  Configure

    ```sh
    poetry config repositories.test-pypi https://test.pypi.org/legacy/
    poetry config pypi-token.test-pypi TOKEN
    poetry config repositories.pypi https://pypi.org/legacy/
    poetry config pypi-token.pypi TOKEN
    ```

2.  Build

    ```sh
    poetry build
    ```

3.  Publish to Test PyPI

    ```sh
    poetry publish -r test-pypi
    ```

4.  Check the test PyPI Package

    ```sh
    python3 -m venv check
    check/bin/pip install -i https://test.pypi.org/simple/ PROJECT-NAME
    check/bin/PROJECT-NAME -V
    rm -rf check
    ```

5.  Deploy to official PyPI:

    ```sh
    poetry publish -r pypi
    ```

6.  Check the official PyPI Package:

    ```sh
    python3 -m venv check
    check/bin/pip install PROJECT-NAME
    check/bin/PROJECT-NAME -V
    rm -rf check
    ```
