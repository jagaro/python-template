#!/bin/bash

set -Eeuo pipefail

cd "$(realpath -m "${BASH_SOURCE[0]}/..")"

bail() {
  echo "Bailing..."
  exit 0
}

echo "> Template Setup <"
echo

read -p "Name (e.g., foo-bar): " NAME
[ -n "$NAME" ] || bail
read -p "Description (e.g., Does something great.): " DESCRIPTION
[ -n "$DESCRIPTION" ] || bail

PYNAME="${NAME//-/_}"

cat <<EOF

This script will now do the following:

  Rename 'PROJECT_NAME' to $PYNAME
  Replace 'PROJECT-NAME' with ${NAME@Q}
  Replace 'PROJECT_NAME' with ${PYNAME@Q}
  Replace 'PROJECT-DESCRIPTION' with ${DESCRIPTION@Q}

EOF

read -p "Enter YES if you wish to proceed? (yes/NO): " CONFIRM
[ "$CONFIRM" = "YES" ] || bail

mv PROJECT_NAME "$PYNAME"

find . -regextype egrep -regex '.*\.(md|py|toml|yml)$' |
  xargs sed -i \
    -e "s/PROJECT-NAME/$NAME/g" \
    -e "s/PROJECT_NAME/$PYNAME/g" \
    -e "s/PROJECT-DESCRIPTION/$DESCRIPTION/g"

# This line is not needed with a lowercase-named project.
sed -i '/^packages = /d' pyproject.toml

rm SETUP-TEMPLATE.sh

echo
echo "Changes complete. Be sure to review the changes!"
