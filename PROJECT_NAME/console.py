import argparse
import sys
from typing import Optional

from . import __description__, __program__, __version__
from .calc import Calc


def get_parser():
    parser = argparse.ArgumentParser(prog=__program__, description=__description__)
    parser.add_argument("value", type=int)
    parser.add_argument("-q", "--quiet", action="store_true", help="suppress output")
    parser.add_argument(
        "-V", "--version", action="version", version=f"{__program__} {__version__}"
    )
    return parser


def main(args: Optional[list] = None):
    args = get_parser().parse_args(args)

    calc = Calc()
    result = calc.double(args.value)

    if not args.quiet:
        print(result)
