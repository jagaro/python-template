try:
    from importlib.metadata import metadata
except ModuleNotFoundError:
    from importlib_metadata import metadata

__metadata = metadata(__name__)

__program__ = __metadata["Name"]
__version__ = __metadata["Version"]
__description__ = __metadata["Summary"]

del __metadata
