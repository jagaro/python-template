# PROJECT-NAME

PROJECT-DESCRIPTION

## Install

```sh
pip install PROJECT-NAME
```

## Usage

```sh
PROJECT-NAME --help
```

## Development

[See the development guide](DEVELOPMENT.md).

## License

[See the license](LICENSE).
